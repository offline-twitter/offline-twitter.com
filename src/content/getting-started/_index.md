---
title: "Getting started"
date: 2024-08-03
weight: 30
---

# Getting started

## 1. Creating a profile

Once you've [installed](/installing) Offline Twitter, you need to create a [profile directory](/concepts#profile-directory).  This is a directory that holds all twitter-related data.

You can call the directory whatever you want.  Suppose I called mine "twitter-data", then:

```bash
twitter create_profile twitter-data
cd twitter-data
```

## 2. Starting the web UI

```bash
twitter webserver --addr localhost:8080 --auto-open
```

Feel free to pick any port that's available; `8080` is the traditional port for non-production HTTP services, but you can use whatever you want.

If it says "bind: address is already in use" and exits, you need to pick a different port.

For security reasons, I suggest only using `localhost` as the host.  The Offline Twitter web UI doesn't have any authentication or multi-user access, so please only deviate from this rule if you understand the implications.  If you stick to `--addr localhost:[some-port]`, you'll be fine.

## 3. That's it

You should be taken to the login page automatically.  If you omitted the `--auto-open` flag, you'll have to open the site manually in your browser.  Click the User Account button in the top left corner to open the login page:

![illustration of login button](login-button.png)

Depicted:
1. login button (opens the login page)
2. login section

That should be all you need to get started and using the app.  Once you login, it will launch an initial scrape to fill your home feed.  This will could take a few seconds to a minute, depending on your internet speed.
