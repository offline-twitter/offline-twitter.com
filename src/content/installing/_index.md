---
title: "Installing"
date: 2024-03-02
weight: 20
bookCollapseSection: true
---

# Installing Offline Twitter

## Requirements

Offline Twitter is all about saving data on your computer, so it's recommended to have at least a few GB of disk space available.

## Installers

Installers are available for Windows and Debian-based systems (Ubuntu, Mint, etc).

For other platforms, send me a message or try building from source.

- [Windows](windows)
- [Linux](linux)

{{< hint info >}}
I don't know how to build for MacOS; their software distribution model confuses me (it's not very Data Sovereignty tbqh).  Also I don't have a Mac and my attempts to obtain a virtualized one using [crime](https://polcompball.miraheze.org/wiki/Piratism) have failed so far.
So, if you want to help me, please get in touch!
{{< /hint >}}


## Building from source

Give it a try!  Offline Twitter is easy to build from source (it only requires a [Go v1.16 or higher](https://go.dev/doc/install) and [sqlite3](https://sqlite.org/)).

### Installing via Go

If you have an up-to-date Go compiler installed (version 1.16 or newer), you can install using `go install`.  Replace the part after the `@` with the latest release version number (check Gitlab for release branches), or just use `@latest` to use the master branch:

```sh
VERSION=0.6.14 # Check Gitlab for newer version tags!
LD_FLAGS="-s -w -X main.version_string=$VERSION -X gitlab.com/offline-twitter/twitter_offline_engine/internal/webserver.use_embedded=true"
go install -ldflags "$LD_FLAGS" gitlab.com/offline-twitter/twitter_offline_engine/cmd/twitter@v$VERSION
```

### Building from scratch

You can also just check out the source code and compile it with `go build`:

- [Build from source](build-from-source)
