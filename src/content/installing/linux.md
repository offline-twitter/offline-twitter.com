---
title: "Installing on Linux"
date: 2024-03-04
weight: 20
---

# Linux installation

## Installing with `apt`

My apt repo is currently under my other domain, `playfulpachyderm.com`.  I will probably move it to `offline-twitter.com` eventually.

Use the following commands to install Offline Twitter.

```sh
# Make sure you have curl and gnupg installed
apt update
apt install -y curl gnupg sudo

# Add my apt repo as a source
echo "deb https://apt.playfulpachyderm.com/ ./" | sudo tee /etc/apt/sources.list.d/offline-twitter.list
curl https://apt.playfulpachyderm.com/KEY.gpg | sudo apt-key add -
apt update

# Install Offline Twitter
apt install offline-twitter

# It will install `twitter` to /usr/local/bin.  Make sure this is in your PATH
twitter  # <-- Should print a big help message
```

You should be able to run Offline Twitter now.  If you system is compatible with with the freedesktop.org standard (aka XDG), shortcuts will have been automatically created; see [Using the app](/using-the-app) for info on how they work.

## Non-`apt` Linux distros

On non-`apt`-compatible distros, the best way to install Offline Twitter is to build it from source.  (You're on an advanced Linux distro, so surely you can handle that, right?)  Alternatively, complain to me and I'll try to make a version for your distro.
