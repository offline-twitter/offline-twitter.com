---
title: "Build From Source"
date: 2024-03-04
weight: 40
---

# Building from source

Wow, you're a real chad!  Congrats on being so cool.

You may wish to check out the source before or after compiling it:

{{< button href="https://gitlab.com/offline-twitter/twitter_offline_engine" >}}Gitlab{{< /button >}}

## Instructions

The build process is automated in a CI build inside a Ubuntu container; you can check out the build file, `/.woodpecker/.build.yml` in the root directory of the repo.

{{< hint warning >}}
I recommend not using the `master` branch, because I develop directly on it.  `release-X.Y.Z` branches are stable builds, `master` may or may not be.
{{< /hint >}}

### Dependencies

- [Go (aka Golang)](https://go.dev/doc/install)

### Build commands

```sh
# Git clone
git clone https://gitlab.com/offline-twitter/twitter_offline_engine.git
cd twitter_offline_engine/cmd

# Compile it with `go build`
go build -ldflags="-s -w -X gitlab.com/offline-twitter/twitter_offline_engine/internal/webserver.use_embedded=true" ./cmd/twitter

# Set it as executable
chmod +x twitter
./twitter
```

You'll get a standalone, statically-linked binary called `twitter` (or `twitter.exe` on Windows), which you can copy onto anywhere in your PATH.  I put it in `/usr/local/bin`, but you can put it somewhere else if you like.

The compiler flag `-X [...]/use_embedded=true` makes the compiler embed the web UI's static files and template files in the output binary, rather than serving them live from disk.  Unless you're making a development build, this is recommended, in order to avoid weird path errors.

