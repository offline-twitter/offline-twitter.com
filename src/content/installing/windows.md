---
title: "Installing on Windows"
date: 2024-03-01
weight: 10
---

# Windows Installation

Download and run the installer:
{{< button href="/downloads/offline-twitter-for-windows.exe" >}}Download{{< /button >}}

After installing, a Start Menu entry should be created called Offline Twitter.

## Using the CLI

If you want to use the CLI, you may have to add it to your Path environment variable.  The path is likely "C:\Program Files\twitter", unless you chose something else during installation.
