---
title: "Home"
date: 2024-03-01
---

# Offline Twitter

{{< hint info >}}
This site and Offline Twitter's source code will continue to refer to the social media platform as "Twitter", regardless of rebranding efforts.  Likewise for the domain, "twitter.com".  It will always be Twitter!
{{< /hint >}}

Offline Twitter is an anti-censorship and digital sovereignty project, aimed at taking back control over your data from centralized tech platforms.  It's an easily self-hosted Twitter client that ships as a single, compact binary.

With Offline Twitter, you can save Twitter content without any dependencies, without worrying about it disappearing.  Enjoy a smooth, clean browsing experience with no inscrutable curation algorithms, no shadowbanning, no "promoted content", no ads or other spyware.

{{< hint warning >}}
This app is in beta release.  It's reasonably stable but probably not perfect.  Expect there to be occasional bugs and hiccups.
{{< /hint >}}

The main purpose of Offline Twitter is to let you own the content you see online.

What does it mean to own data?  It means:

- You have a full copy of it
- It lasts until you decide to delete it
- You can do whatever you want with it, including opening it with other apps

## Installing

Installers and instructions are available on the [Installing](/installing) page.

## Why Offline Twitter?

I wanted to create a way to save twitter content without any dependencies, so I could view it any time.  There are several reasons why I wanted to do this.  Here are some of them:

1. The twitter web client is garbage:
	- it uses a ton of memory
	- it's slow
	- it's full of spyware and ads

1. There's this weird "algorithm" that nobody knows what it is, but for some reason it prevents you from seeing tweets
from people you're subscribed to.

1. Twitter is quite censorious.  A lot of great content is being lost as Twitter bans people all over
the place.  I wanted to create a way to save a personal copy of everything on Twitter that I liked, without worrying
about it disappearing.

Check out the [Introduction](/introduction) page to learn more about Offline Twitter, what it does and how it works.

<!-- ## Getting Started

If you've downloaded Offline Twitter, but can't figure out how to use it because I suck at UX, the [Getting Started](/getting-started) page will help.
 -->
## Development

See the [development page](/development).
