---
title: "Problems and solutions"
date: 2024-03-01
weight: 60
---

# Problems and solutions

This is a list of issues people have had and solutions / workarounds.

## Login not working

Authenticating with Twitter may cause a secondary verification challenge (e.g., 2FA), especially if you're logging in frequently, from a new device, or otherwise something unusual.  Offline Twitter will try to intercept these and issue a secondary prompt, but may not handle all possible variations correctly.

If logging in fails unexpectedly:
- check that you can log in on the Twitter website, ideally from the same device, and clear any secondary verification challenges there
- then try logging in through Offline Twitter again.
