---
title: "Introduction"
date: 2024-03-01
weight: 10
---

# Introduction

Offline Twitter is an easily self-hosted client for browsing Twitter, which automatically saves a copy of everything you view.

{{< hint info >}}
##### Note

Offline Twitter is mainly meant for reading, not posting or other actions.  There is a small set of interaction features supported (e.g., "liking" tweets, sending DMs), and more will be added over time.  To post tweets, reply or retweet, just open regular Twitter.  Every page and tweet has an "Open on twitter.com" button:

![button](./open-external-buttons.png)
{{< /hint >}}

## Data Architecture

All the data is stored in a big folder on your computer called a *profile*.  Structured data (e.g., tweets, retweets, users, attachment metadata, etc.) is stored in a SQLite database called `twitter.db`, with a logical and straightforward format.  Images and videos are stored as regular files.  This format is designed to be maximally transparent and easy to use with other applications.

![Offline Twitter block diagram](./offline-twitter-diagram.svg)

The main goal of Offline Twitter is data sovereignty, so all the pieces of the app are built around this storage format.

In particular, the built-in web GUI only reads from the local database.  Any time you open a new tweet, for instance, it first loads the tweet from Twitter and saves it in the database, and only then loads it into the UI.  This ensures that anything you see in the UI is already permanently saved.

This architecture has a bonus advantage, besides data ownership, which is that you can look at old tweets (i.e., anything already saved) without needing an internet connection.  Of course, you won't be able to fetch new tweets until you are connected again.  This is the origin of the name "Offline" Twitter, i.e., it has a completely functional offline mode.

## Application architecture

Offline Twitter is a single, self-contained, all-included static binary, including the web UI.  The binary can be used directly to fetch data from Twitter and save it (e.g.: `twitter fetch_tweet https://twitter.com/Offline_Twatter/status/1762239926437843421`, or it can be used to serve the web UI (i.e., `twitter webserver`).

The web UI doesn't use Electron; it's meant to be opened in a normal browser tab, using your normal web browser.

## Comparison to regular (i.e., online) Twitter

Offline Twitter has a number of user experience advantages over the Twitter web UI or mobile app.

- Pages are light and load fast (almost no Javascript)
- No ads or spyware
- Videos don't auto-play, freeze or skip
