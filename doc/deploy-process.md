# Init

On remote:
```sh
git init --bare ~/offline-twitter.com
# copy from `scripts/post-receive` to `~/offline-twitter.com/hooks/post-receive`
chmod +x ~/offline-twitter.com/hooks/post-receive
```

On local:
```sh
git remote add production offline-twitter@offline-twitter.com:/home/offline-twitter/offline-twitter.com
````

# Deploying

git commit
git push production
